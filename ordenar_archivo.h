#ifndef TP2_PARTICIONAR_ARCHIVO_H
#define TP2_PARTICIONAR_ARCHIVO_H

#include <stdlib.h>
#include <stdbool.h>
#include "utils.h"


/*
 * Ordena el archivo pasado por parámetro, utilizando particiones que ocupan
 * a lo sumo max_bytes bytes.
 */
bool ordenar_archivo(char* archivo_origen, char* archivo_destino,size_t max_bytes, cmp_linea_func_t cmp);

#endif //TP2_PARTICIONAR_ARCHIVO_H
