#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "strutil.h"
#include "abb.h"
#include "hash.h"
#include "cola.h"
#include "heap.h"
#include "lista.h"
#define TIME_FORMAT "%FT%T%z"


size_t cantidad_parametros(char* linea) {
    size_t cant_parametros = 0;
    size_t len_str = strlen(linea);
    for (int i = 0; i < len_str; i++) {
        if (linea[i]==' ' || linea[i]=='\n')
            cant_parametros++;
    }
    return cant_parametros;
}


char* str_copy(const char* str){
    char* copia = malloc(1 + strlen(str));
    if(!copia){
        return NULL;
    }

    strcpy(copia, str);
    return copia;
}

size_t* str_to_sizet(char* string){
    char* endptr;
    size_t convertido = strtoul(string, &endptr, 10);

    if(*endptr != '\0'){
        return NULL;
    }

    size_t* resultado = malloc(sizeof(size_t));
    if(resultado == NULL){
        return NULL;
    }
    *resultado = convertido;
    return resultado;
}

int* convertir_ip(const char* dir_ip) {
    char** arr_ip = split(dir_ip, '.');
    int* ip = malloc(sizeof(int)*4);
    for (int i = 0; i < 4; i++)
        ip[i] = atoi(arr_ip[i]);

    free_strv(arr_ip);
    return ip;
}

int comparar_ips(const char* ip_1, const char* ip_2) {
    int* ip1 = convertir_ip(ip_1);
    int* ip2 = convertir_ip(ip_2);

    int resultado = 0;
    for (int i = 0; i < 4; i++) {
        if (ip1[i] < ip2[i]) {
            resultado = -1;
            break;
        } else if (ip1[i] > ip2[i]) {
            resultado = 1;
            break;
        }
    }

    free(ip1);
    free(ip2);
    return resultado;
}

int comparar_ips_invertida(const void* ip_1, const void* ip_2) {
    return comparar_ips(ip_1, ip_2) * -1;
}

int comparar_lineas_log(const void* linea, const void* otra_linea){

    char** splitted_1 = split(linea, '\t');
    if(splitted_1 == NULL) {
        return -1;
    }

    char** splitted_2 = split(otra_linea, '\t');
    if(splitted_2 == NULL){
        free_strv(splitted_1);
        return -1;
    }

    const size_t POS_FECHA = 1;
    char* f1 = splitted_1[POS_FECHA];
    char* f2 = splitted_2[POS_FECHA];
    int cmp_fecha = strcmp(f1, f2);
    if(cmp_fecha != 0){
        free_strv(splitted_1);
        free_strv(splitted_2);
        return cmp_fecha;
    }

    const size_t POS_IP = 0;
    const char* ip1 = splitted_1[POS_IP];
    const char* ip2 = splitted_2[POS_IP];
    int cmp_ip = comparar_ips(ip1, ip2);
    if(cmp_ip != 0){
        free_strv(splitted_1);
        free_strv(splitted_2);
        return cmp_ip;
    }

    const size_t POS_URL = 3;
    const char* url1 = splitted_1[POS_URL];
    const char* url2 = splitted_2[POS_URL];
    int cmp_url = strcmp(url1, url2);

    free_strv(splitted_1);
    free_strv(splitted_2);

    return cmp_url;
}

