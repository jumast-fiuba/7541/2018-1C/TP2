CC = gcc
CFLAGS = -g -std=c99 -Wall -Wconversion -Wno-sign-conversion -Wbad-function-cast
CFLAGS += -Wshadow -Wpointer-arith -Wunreachable-code -Wformat=2 -Werror
CFLAGS += -Wunused-result
CGLAGS += -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = analog
ZIPNAME = tp2_entrega.zip
TEST_FOLDER = test_files

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

TP2_OBJECTS = main.o
TP2_OBJECTS += ordenar_archivo.o agregar_archivo.o ver_visitantes.o
TP2_OBJECTS += abb.o cola.o hash.o heap.o lista.o pila.o strutil.o
TP2_OBJECTS += file_reader.o utils.o particion.o

build: $(TP2_OBJECTS)
	$(CC) $(CFLAGS) -o $(EXEC) $(TP2_OBJECTS)
	cp $(EXEC) $(TEST_FOLDER)/
	$(MAKE) clean
	
run: build
	
	$(MAKE) clean

MAXSIZE = 1
val: build
	valgrind $(VALFLAGS) $(TEST_FOLDER)/$(EXEC) $(MAXSIZE)

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

ENTREGA = main.c Makefile
ENTREGA += agregar_archivo.c agregar_archivo.h ordenar_archivo.c ordenar_archivo.h ver_visitantes.c ver_visitantes.h
ENTREGA += abb.c abb.h cola.c cola.h hash.c hash.h heap.c heap.h lista.c lista.h pila.c pila.h strutil.c strutil.h
ENTREGA += file_reader.c file_reader.h utils.c utils.h particion.c particion.h
ENTREGA += deps.mk

zip:
	zip $(ZIPNAME) $(ENTREGA)




