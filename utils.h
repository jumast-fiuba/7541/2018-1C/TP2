#ifndef TP2_UTILS_H
#define TP2_UTILS_H


/*
 * Cuenta la cantidad de parámetros recibidos por entrada estándar.
 */
size_t cantidad_parametros(char* linea);

/*
 * Declaración de tipo para función de comparación de líneas de un archivo
 * de log.
 */
typedef int (*cmp_linea_func_t)(const void* linea_1, const void* linea_2);

/*
 * Copia la cadena pasada por parámetro.
 * Si falla malloc devuelve NULL.
 */
char* str_copy(const char* str);

size_t* str_to_sizet(char* string);

/* Convierte una dirección IP recibida por parámetro en un arreglo de enteros,
 * constituido por los cuatro bloques numéricos de la dirección IP.
 */
int* convertir_ip(const char* dir_ip);

/* Función de comparación que recibe dos cadenas con una dirección IP, y
 * devuelve:
 *   menor a 0  si  ip_1 < ip_2
 *       0      si  ip_1 == ip_2
 *   mayor a 0  si  ip_1 > ip_2
 */
int comparar_ips(const char* ip_1, const char* ip_2);

/* Función de comparación invertida que recibe dos cadenas con una dirección IP, y
 * devuelve:
 *   menor a 0  si  ip_1 > ip_2
 *       0      si  ip_1 == ip_2
 *   mayor a 0  si  ip_1 < ip_2
 */
int comparar_ips_invertida(const void* ip_1, const void* ip_2);

/* Función de comparación que recibe dos líneas de un archivo de log
 * y devuelve:
 *   menor a 0  si  linea_1 < linea_2
 *       0      si  linea_1 == linea_2
 *   mayor a 0  si  linea_1 > linea_2
 *
 *   La comparación se hace primero por fecha, luego por ip, y luego por url.
 */
int comparar_lineas_log(const void* linea_1, const void* linea_2);

#endif //TP2_UTILS_H
