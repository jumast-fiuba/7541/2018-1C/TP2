#ifndef TP2_VER_VISITANTES_H
#define TP2_VER_VISITANTES_H

#include "abb.h"

/* Muestra por pantalla una lista de todas las IPs dentro de un rango dado, que
 * realizaron alguna petición.
 */
void ver_visitantes(abb_t* ips, char* ip_inicial, char* ip_final);

#endif //TP2_VER_VISITANTES_H
