#ifndef TP2_AGREGAR_ARCHIVO_H
#define TP2_AGREGAR_ARCHIVO_H

#include "abb.h"

/* Procesa un archivo ordenado por tiempo, actualizando las direcciones IP
 * que han accedido a un recurso. Detecta los posibles casos de ataques de
 * denegación de servicio.
 */
bool agregar_archivo(abb_t* ips, char* nombre_archivo);

#endif //TP2_AGREGAR_ARCHIVO_H
