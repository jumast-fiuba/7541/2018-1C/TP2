#include "pila.h"
#include <stdlib.h>

/* Definición del struct pila proporcionado por la cátedra.*/
struct pila {
    void** datos;
    size_t cantidad;  // Cantidad de elementos almacenados.
    size_t capacidad;  // Capacidad del arreglo 'datos'.
};

/* *****************************************************************
 *                    PRIMITIVAS DE LA PILA
 * *****************************************************************/

pila_t* pila_crear(void) {
	pila_t* pila = malloc(sizeof(pila_t));
	if (pila == NULL)
		return NULL;

	pila->cantidad = 0;
	pila->capacidad = 10;
	
	pila->datos = malloc(pila->capacidad * sizeof(void*));
	if (pila->datos == NULL) {
		free(pila);
		return NULL;
	}

	return pila;
}

void pila_destruir(pila_t *pila) {
	free(pila->datos);
	free(pila);
	return;
}

bool pila_esta_vacia(const pila_t *pila) {
	return (pila->cantidad == 0);
}

bool pila_apilar(pila_t *pila, void* valor) {
	if (pila->cantidad >= pila->capacidad) {
		size_t capacidad_nueva = pila->capacidad * 2;
		void** datos_nuevo = realloc(pila->datos, capacidad_nueva*sizeof(void*));
		if (datos_nuevo == NULL) {
			return false;
		}
		pila->capacidad = capacidad_nueva;
		pila->datos = datos_nuevo;
	}

	pila->cantidad++;
	pila->datos[pila->cantidad-1] = valor;
	return true;
}

void* pila_ver_tope(const pila_t *pila) {
	if (pila_esta_vacia(pila))
		return NULL;

	return pila->datos[pila->cantidad-1];
}

void* pila_desapilar(pila_t *pila) {
	if (pila_esta_vacia(pila))
		return NULL;
	
	void* valor_desapilado = pila_ver_tope(pila);
	pila->cantidad--;
	
	if (pila->cantidad < pila->capacidad/4) {
		size_t capacidad_nueva = pila->capacidad / 2;
		void** datos_nuevo = realloc(pila->datos, capacidad_nueva*sizeof(void*));
		if (datos_nuevo != NULL) {
			pila->capacidad = capacidad_nueva;
			pila->datos = datos_nuevo;
		}
	}

	return valor_desapilado;
}