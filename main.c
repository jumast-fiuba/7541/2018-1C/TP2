#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdbool.h>
#include "ordenar_archivo.h"
#include "strutil.h"
#include "stdio.h"
#include "string.h"
#include "cola.h"
#include "abb.h"
#include "utils.h"
#include "ordenar_archivo.h"
#include "agregar_archivo.h"
#include "ver_visitantes.h"
#include "lista.h"
#include "time.h"

#define ARGV_CANTIDAD_ESPERADA 2
#define ARGV_MEMORIA 1

// para todos los comandos
#define NOMBRE_COMANDO 0

// para ordenar_archivo
#define CANT_PARAMETROS_ORDENAR 3
#define ARCHIVO_ORIGEN 1
#define ARCHIVO_DESTINO 2

// para agregar_archivo
#define CANT_PARAMETROS_AGREGAR 2
#define NOMBRE_ARCHIVO 1

// para ver_visitantes
#define CANT_PARAMETROS_VISITANTES 3
#define IP_INICIAL 1
#define IP_FINAL 2

int main(int argc, char* argv[]) {

    if (argc != ARGV_CANTIDAD_ESPERADA) {
        return EXIT_FAILURE;
    }

    size_t *max_size_kb = str_to_sizet(argv[ARGV_MEMORIA]);
    if (max_size_kb == NULL) {
        return EXIT_FAILURE;
    }
    size_t max_size_bytes = *max_size_kb * 1000;
    free(max_size_kb);

    abb_t *ips = abb_crear(comparar_ips, free);
    if (ips == NULL) {
        return EXIT_FAILURE;
    }

    size_t numero_bytes = 0;
    char *linea = NULL;
    ssize_t bytes_leidos;

    while ((bytes_leidos = getline(&linea, &numero_bytes, stdin)) >1) {
        size_t cant_parametros = cantidad_parametros(linea);
        char** parametros = split(linea, ' ');
        if(parametros == NULL){
            free(linea);
            abb_destruir(ips);
            return EXIT_FAILURE;
        }

        bool ok = false;
        char* nombre_comando = parametros[NOMBRE_COMANDO];

        if (strcmp(nombre_comando, "ordenar_archivo")==0 && cant_parametros==CANT_PARAMETROS_ORDENAR) {
            ok = ordenar_archivo(parametros[ARCHIVO_ORIGEN], parametros[ARCHIVO_DESTINO], max_size_bytes, comparar_lineas_log);
        
        } else if (strcmp(nombre_comando, "agregar_archivo")==0 && cant_parametros==CANT_PARAMETROS_AGREGAR) {
            ok = agregar_archivo(ips, parametros[NOMBRE_ARCHIVO]);
        
        } else if (strcmp(nombre_comando, "ver_visitantes")==0 && cant_parametros==CANT_PARAMETROS_VISITANTES) {
            ver_visitantes(ips, parametros[IP_INICIAL], parametros[IP_FINAL]);
            ok = true;
        }

        if (ok) {
            fprintf(stdout, "OK\n");
        } else {
            fprintf(stderr, "Error en comando %s\n", parametros[NOMBRE_COMANDO]);
            abb_destruir(ips);
            free_strv(parametros);
            free(linea);
            return EXIT_SUCCESS;
        }

        free_strv(parametros);
    }

    free(linea);
    abb_destruir(ips);
    return EXIT_SUCCESS;
}