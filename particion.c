#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lista.h"
#include "utils.h"
#include "particion.h"
#include "heap.h"

/* ******************************************************************
 *               Implementación de funciones auxiliares
 * *****************************************************************/

/* nombre: nombre del prefijo del archivo, que puede ser el nombre del archivo original
particion: numero de particion a crear
Por ejemplo: crear_nombre_particion("access001.log", 7) --> "access001.log_07"
*/
static char* crear_nombre_particion(const char* nombre, size_t particion) {
    char* nombre_archivo = malloc(sizeof(char) * (strlen(nombre) + 5));
    sprintf(nombre_archivo, "%s_%02zu", nombre, particion);
    return nombre_archivo;
}

/* ******************************************************************
 *             particion_t: implementación de primitivas
 * *****************************************************************/

particion_t* particion_crear(char* nombre_de_archivo, size_t numero_de_particion, size_t max_size_in_bytes){

    particion_t* particion = malloc(sizeof(particion_t));
    if(particion == NULL){
        return NULL;
    }

    char* nombre_particion = crear_nombre_particion(nombre_de_archivo, numero_de_particion);
    if(nombre_particion == NULL){
        free(particion);
        return NULL;
    }

    lista_t* lista = lista_crear();
    if(lista == NULL){
        free(particion);
        free(nombre_particion);
        return NULL;
    }

    particion->nombre_archivo = nombre_particion;
    particion->lineas = lista;
    particion->max_size_in_bytes = max_size_in_bytes;
    particion->size_in_bytes = 0;

    return particion;
}

void particion_destruir(particion_t* particion){
    free(particion->nombre_archivo);
    lista_destruir(particion->lineas, free);
    free(particion);
}

char* particion_nombre_de_archivo(particion_t* particion){

    char* copia = str_copy(particion->nombre_archivo);
    if(copia == NULL){
        return NULL;
    }

    return copia;
}

bool particion_acepta_linea(particion_t* particion, const char* linea){

    size_t len = strlen(linea);
    size_t size = len * sizeof(char);
    bool acepta = particion->size_in_bytes + size <= particion->max_size_in_bytes;
    return acepta;
}

bool particion_agregar_linea(particion_t* particion, char* linea){

    char* copia_linea = str_copy(linea);
    if(copia_linea == NULL){
        return false;
    }

    particion->size_in_bytes += strlen(linea) * sizeof(char);
    return lista_insertar_ultimo(particion->lineas, copia_linea);
}

bool particion_guardar_ordenada(particion_t* particion, cmp_linea_func_t cmp){

    size_t cantidad_de_lineas = lista_largo(particion->lineas);
    char** lineas = malloc(cantidad_de_lineas * sizeof(char*));
    if(lineas == NULL){
        return false;
    }

    lista_iter_t* lista_iter = lista_iter_crear(particion->lineas);
    if(lista_iter == NULL){
        free(lineas);
        return false;
    }

    // list -> char**
    size_t i = 0;
    while(!lista_iter_al_final(lista_iter)){
        char* linea = lista_iter_ver_actual(lista_iter);
        lineas[i] = linea;
        lista_iter_avanzar(lista_iter);
        i++;
    }
    lista_iter_destruir(lista_iter);

    // ordenar
    heap_sort((void**)lineas, cantidad_de_lineas, cmp);

    // guardar
    FILE* archivo = fopen(particion->nombre_archivo, "w");
    if(archivo == NULL){
        free(lineas);
        return false;
    }
    for(size_t j = 0; j < cantidad_de_lineas; j++){
        char* linea = lineas[j];
        fputs(linea, archivo);
    }

    free(lineas);
    fclose(archivo);
    return true;
}

bool particion_guardar(particion_t* particion){

    FILE* archivo = fopen(particion->nombre_archivo, "w");
    if(archivo == NULL){
        return false;
    }

    lista_iter_t* lista_iter = lista_iter_crear(particion->lineas);
    if(lista_iter == NULL){
        fclose(archivo);
        return false;
    }

    while(!lista_iter_al_final(lista_iter)){
        char* linea = lista_iter_ver_actual(lista_iter);
        fputs(linea, archivo);
        lista_iter_avanzar(lista_iter);
    }

    lista_iter_destruir(lista_iter);
    fclose(archivo);

    return true;
}