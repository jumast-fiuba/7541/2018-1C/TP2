#include "hash.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define TAM 59
#define FACTOR_REDIMENSION_MAXIMO 0.8
#define FACTOR_REDIMENSION_MINIMO 0.1

typedef enum estado {
	ocupado,
	borrado
} estado_t;

/* Definición del struct nodo_hash */
typedef struct nodo_hash {
	char* clave;  
	void* dato;  
    estado_t estado;
} nodo_hash_t;


/* Definición del hash ecerrado */
struct hash {
    nodo_hash_t** tabla;
    size_t tam;
    size_t ocupados;
    size_t borrados;
    hash_destruir_dato_t destruir_dato;
};


/* Definición del iterador del hash cerrado */
struct hash_iter {
	const hash_t* hash;
	size_t posicion;
};


/* *****************************************************************
 *                    FUNCION DE HASHING
 * *****************************************************************/

size_t hashing(const char* clave) {
	size_t i;
	for (i=0; *clave!='\0'; clave++)
		i = 131*i + *clave;
	return i;
}


/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/

/* Devuelve la posición donde debería estar una clave en una tabla de hash.
 * Pre: La estructura hash fue inicializada
 */
size_t hash_buscar_posicion(const hash_t* hash, const char* clave) {
	size_t posicion = hashing(clave)%hash->tam;
	
	while (hash->tabla[posicion]!=NULL) {
		if (strcmp(hash->tabla[posicion]->clave, clave) == 0)
			break;
		posicion++;
		if (posicion == hash->tam)
			posicion = 0;
	}
	return posicion;
}

/* Crea un nodo nuevo con la clave y dato recibidos por parámetro.
 */
nodo_hash_t* crear_nodo_hash(const char* clave, void* dato) {
	nodo_hash_t* nodo_nuevo = malloc(sizeof(nodo_hash_t));
	if (nodo_nuevo == NULL)
		return NULL;

	nodo_nuevo->clave = malloc(strlen(clave)+1);
	if (nodo_nuevo->clave == NULL)
		return NULL;
	strcpy(nodo_nuevo->clave, clave);
	
	nodo_nuevo->dato = dato;
	nodo_nuevo->estado = ocupado;

	return nodo_nuevo;
}


/* Reemplaza el dato guardado en un nodo. Aumenta la cantidad de ocupados en el hash.
 * Pre: la estructura fue creada y el nodo está guardado en el hash.
 */
bool reemplazar_dato(hash_t* hash, nodo_hash_t* nodo, void* dato) {
	if ((hash->destruir_dato) && (nodo->estado!=borrado))
		hash->destruir_dato(nodo->dato);
	
	nodo->dato = dato;
	if (nodo->estado == borrado) {
		nodo->estado = ocupado;	
		hash->ocupados++;
	}
	
	return true;
}


/* Redimensiona la tabla de hash a un tamaño nuevo recibido por parámetro.
 * Pre: La estructura hash fue inicializada
 * Post: La tabla de hash tiene el tamaño nuevo.
 */
void hash_redimensionar(hash_t* hash, size_t tam_anterior, size_t tam_nuevo) {
	nodo_hash_t** tabla_anterior = hash->tabla;
	nodo_hash_t** tabla_nueva = malloc(sizeof(nodo_hash_t*)*tam_nuevo);
	if (tabla_nueva == NULL)
		return;

	hash->tabla = tabla_nueva;
	hash->tam = tam_nuevo;
		
	for (int i = 0; i < hash->tam; i++)
		hash->tabla[i]=NULL;

	hash->ocupados = 0;
	hash->borrados = 0;

	for (int i = 0; i < tam_anterior; i++) {
		if (tabla_anterior[i]!=NULL) {
			if (tabla_anterior[i]->estado!=borrado)
				hash_guardar(hash, tabla_anterior[i]->clave, tabla_anterior[i]->dato);

			free(tabla_anterior[i]->clave);
			free(tabla_anterior[i]);
		}
	}
	free(tabla_anterior);
	return;
}


/* *****************************************************************
 *                    PRIMITIVAS DEL HASH
 * *****************************************************************/

hash_t* hash_crear(hash_destruir_dato_t destruir_dato) {
	hash_t* hash = malloc(sizeof(hash_t));
	if (hash == NULL)
		return NULL;
	
	hash->tabla = malloc(sizeof(nodo_hash_t*)*TAM);  //Reserva espacio para cada nodo
	if (hash->tabla == NULL) {
		free(hash);
		return NULL;
	}

	hash->tam = TAM;
	for (int i = 0; i < hash->tam; i++)
		hash->tabla[i]=NULL; 

	hash->ocupados = 0;
	hash->borrados = 0;
	hash->destruir_dato = destruir_dato;
	return hash;
}

bool hash_guardar(hash_t* hash, const char* clave, void* dato) {
	//Verifico si hay que redimensionar
	float factor_carga = (float)(hash->ocupados + hash->borrados)/(float)hash->tam;
	if (factor_carga >= FACTOR_REDIMENSION_MAXIMO) {
		size_t tam_nuevo = hash->tam *2;
		hash_redimensionar(hash, hash->tam, tam_nuevo);
	}

	//Encuentro posición de la clave en la estructura
	size_t posicion = hash_buscar_posicion(hash, clave);
	
	//Si la clave ya está en la estructura, reemplazo el dato
	if ((hash->tabla[posicion]!=NULL) && (strcmp(hash->tabla[posicion]->clave, clave) == 0))
		return reemplazar_dato(hash, hash->tabla[posicion], dato);

	//Se crea un nuevo nodo
	nodo_hash_t* nodo_nuevo = crear_nodo_hash(clave, dato);
	if (nodo_nuevo == NULL)
		return false;

	hash->tabla[posicion] = nodo_nuevo;
	hash->ocupados++;

	return true;
}



void* hash_borrar(hash_t* hash, const char* clave) {
	if (hash_pertenece(hash, clave)==false)
		return NULL;
	
	size_t posicion = hash_buscar_posicion(hash, clave); 

	void* dato_a_borrar = hash->tabla[posicion]->dato;

	hash->tabla[posicion]->estado = borrado;
	hash->ocupados--;
	hash->borrados++;

	float factor_carga = (float)(hash->ocupados + hash->borrados)/(float)hash->tam;
	if (factor_carga <= FACTOR_REDIMENSION_MINIMO) {
		size_t tam_nuevo = hash->tam /2;
		hash_redimensionar(hash, hash->tam, tam_nuevo);
	}

	return dato_a_borrar;
}
 


void* hash_obtener(const hash_t* hash, const char* clave) {
	if (hash_pertenece(hash, clave)==false)
		return NULL;

	return hash->tabla[hash_buscar_posicion(hash, clave)]->dato;
}


bool hash_pertenece(const hash_t* hash, const char* clave) {
	size_t posicion = hash_buscar_posicion(hash, clave);
	
	return (hash->tabla[posicion]) && (hash->tabla[posicion]->estado!=borrado);
}


size_t hash_cantidad(const hash_t* hash) {
	return hash->ocupados;
}


void hash_destruir(hash_t* hash) {
	for (int i = 0; i < hash->tam; i++) {
		if (hash->tabla[i]!=NULL) {
			if ((hash->destruir_dato) && (hash->tabla[i]->estado!=borrado))
				hash->destruir_dato(hash->tabla[i]->dato);
			free(hash->tabla[i]->clave);
			free(hash->tabla[i]);
		}
	}
	free(hash->tabla);
	free(hash);
	return;
}



/* *****************************************************************
 *                    PRIMITIVAS DEL ITERADOR DEL HASH
 * *****************************************************************/

/************** FUNCION AUXILIAR  **************************/

/* Devuelve la posición del iterador del hash.
 * Itera hasta encontrar una posición válida.
 * Pre: el iterador fue creado.
 */
size_t posicion_iterador(hash_iter_t* iter, size_t posicion) {
	while ((posicion < iter->hash->tam) && (iter->hash->tabla[posicion]==NULL))
		posicion++;
	return posicion;	
}

hash_iter_t* hash_iter_crear(const hash_t* hash) {
	hash_iter_t* iter = malloc(sizeof(hash_iter_t));
	if (iter==NULL)
		return NULL;
	
	iter->hash = hash;
	iter->posicion = posicion_iterador(iter, 0);
	
	return iter;
}


bool hash_iter_avanzar(hash_iter_t* iter) {
	if (hash_iter_al_final(iter))
		return false;
	iter->posicion = posicion_iterador(iter, iter->posicion +1);

	return true;
}


const char* hash_iter_ver_actual(const hash_iter_t* iter) {
	if (hash_iter_al_final(iter))
		return NULL;
	
	return iter->hash->tabla[iter->posicion]->clave;
}


bool hash_iter_al_final(const hash_iter_t* iter) {
	return iter->posicion==iter->hash->tam;
}


void hash_iter_destruir(hash_iter_t* iter) {
	free(iter);
	return;
}
