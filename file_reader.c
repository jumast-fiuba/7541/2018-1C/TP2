#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "file_reader.h"


/* ******************************************************************
 *             file_reader_t: implementación de primitivas
 * *****************************************************************/

file_reader_t* file_reader_crear(char* nombre_de_archivo){
    FILE* archivo = fopen(nombre_de_archivo, "r");
    if(archivo == NULL){
        return NULL;
    }

    file_reader_t* file_reader = malloc(sizeof(file_reader_t));
    if(file_reader == NULL){
        fclose(archivo);
        return NULL;
    }

    file_reader->archivo = archivo;
    file_reader->linea = NULL;
    file_reader->size_in_bytes = 0;
    file_reader->eof = false;
    file_reader->error = false;

    return file_reader;
}

void file_reader_destruir(file_reader_t* file_reader){
    fclose(file_reader->archivo);
    free(file_reader->linea);
    free(file_reader);
}

bool file_reader_leer(file_reader_t* file_reader){

    if(file_reader->error || file_reader->eof){
        return false;
    }

    size_t leidos = getline(&file_reader->linea, &file_reader->size_in_bytes, file_reader->archivo);
    file_reader->leidos = leidos;
    if(leidos == -1){
        bool eof = feof(file_reader->archivo) != 0 ? true :false;
        if(eof){
            file_reader->eof = true;
        }
        else{
            file_reader->error = true;
        }
        return false;
    }

    return true;
}