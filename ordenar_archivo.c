#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "strutil.h"
#include "heap.h"
#include "lista.h"
#include "ordenar_archivo.h"
#include "utils.h"
#include "file_reader.h"
#include "particion.h"

/* ******************************************************************
 *                heap_item_t: estructura auxiliar
 * *****************************************************************/

typedef struct {
    char* linea;
    size_t numero_archivo;
} heap_item_t;

static heap_item_t* heap_item_crear(char* linea, size_t numero_archivo);
static void heap_item_destruir(heap_item_t* heap_item);
static int heap_item_comparar(const void *item1, const void *item2);

/* ******************************************************************
 *               Declaración de funciones auxiliares
 * *****************************************************************/

static lista_t* particionar_ordenando(char *nombre_archivo, size_t max_bytes, cmp_linea_func_t cmp);
static bool juntar_ordenados(char *nombre_de_archivo, lista_t *particiones_nombres, cmp_linea_func_t cmp);
static bool borrar_particiones(lista_t* particiones);


/* ******************************************************************
 *                        ORDENAR ARCHIVO
 * *****************************************************************/

bool ordenar_archivo(char* archivo_origen, char* archivo_destino,size_t max_bytes, cmp_linea_func_t cmp){
    lista_t* particiones_ordenadas = particionar_ordenando(archivo_origen, max_bytes, cmp);
    if (particiones_ordenadas == NULL)
        return false;

    bool juntar_ok = juntar_ordenados(archivo_destino, particiones_ordenadas, heap_item_comparar);
    if(!juntar_ok){
        lista_destruir(particiones_ordenadas, free);
        return false;
    }

    borrar_particiones(particiones_ordenadas);
    lista_destruir(particiones_ordenadas, free);

    return true;
}


/* ******************************************************************
 *              Implementación de funciones auxiliares
 * *****************************************************************/

static void lista_destruir_adapter(void* dato){
    particion_t* particion = dato;
    particion_destruir(particion);
}

static void heap_destruir_adapter(void* dato){
    heap_item_t* heap_item = dato;
    heap_item_destruir(heap_item);
}

static lista_t* particionar_ordenando(char *nombre_archivo, size_t max_bytes, cmp_linea_func_t cmp){
    file_reader_t* file_reader = file_reader_crear(nombre_archivo);
    if(file_reader == NULL){
        return NULL;
    }

    lista_t* particiones = lista_crear();
    if(particiones == NULL){
        file_reader_destruir(file_reader);
        return NULL;
    }

    size_t numero_de_particion = 1;
    particion_t* particion = particion_crear(nombre_archivo, numero_de_particion, max_bytes);
    if(particion == NULL){
        file_reader_destruir(file_reader);
        lista_destruir(particiones, NULL);
        return NULL;
    }

    file_reader_leer(file_reader);
    if(file_reader->error){
        file_reader_destruir(file_reader);
        lista_destruir(particiones, NULL);
        particion_destruir(particion);
        return NULL;
    }

    while(!file_reader->eof){

        char* linea = file_reader->linea;
        if(!particion_acepta_linea(particion, linea)){
            bool insertar_ok = lista_insertar_ultimo(particiones, particion_nombre_de_archivo(particion));
            bool guardar_ok = particion_guardar_ordenada(particion, cmp);
            if(!insertar_ok || !guardar_ok){
                file_reader_destruir(file_reader);
                lista_destruir(particiones, lista_destruir_adapter);
                particion_destruir(particion);
                return NULL;
            }

            particion_destruir(particion);
            numero_de_particion++;

            particion = particion_crear(nombre_archivo, numero_de_particion, max_bytes);
            if(particion == NULL){
                file_reader_destruir(file_reader);
                lista_destruir(particiones, lista_destruir_adapter);
                return NULL;
            }
        }

        bool agregar_ok = particion_agregar_linea(particion, linea);
        file_reader_leer(file_reader);

        if(!agregar_ok || file_reader->error){
            file_reader_destruir(file_reader);
            lista_destruir(particiones, lista_destruir_adapter);
            return NULL;
        }
    }

    bool insertar_ultima_ok = lista_insertar_ultimo(particiones, particion_nombre_de_archivo(particion));
    bool guardar_ultima_ok = particion_guardar_ordenada(particion, cmp);

    particion_destruir(particion);
    file_reader_destruir(file_reader);

    if (insertar_ultima_ok && guardar_ultima_ok){
        return particiones;
    }
    else{
        lista_destruir(particiones, lista_destruir_adapter);
        return NULL;
    }
}

static void free_file_reader_vec(file_reader_t** readers, size_t n){
    for(size_t i = 0; i < n; i++){
        file_reader_destruir(readers[i]);
    }
    free(readers);
}

static file_reader_t** crear_file_reader_vec(lista_t* particiones_nombres){
    size_t cant_de_particiones = lista_largo(particiones_nombres);
    file_reader_t** archivos = malloc(cant_de_particiones * sizeof(file_reader_t*));
    if(archivos == NULL){
        return NULL;
    }

    lista_iter_t* lista_iter = lista_iter_crear(particiones_nombres);
    if(lista_iter == NULL){
        free(archivos);
        return NULL;
    }

    size_t  i = 0;
    while(!lista_iter_al_final(lista_iter)){
        char* nombre_de_particion = lista_iter_ver_actual(lista_iter);
        file_reader_t* file_reader = file_reader_crear(nombre_de_particion);
        if(file_reader == NULL){
            free_file_reader_vec(archivos, i);
            return NULL;
        }

        archivos[i] = file_reader;
        lista_iter_avanzar(lista_iter);
        i++;
    }
    lista_iter_destruir(lista_iter);

    return archivos;
}

static void juntar_ordenados_clean_up(FILE* archivo_ordenado, heap_t* heap, file_reader_t** file_reader_vec, size_t cant_particiones){
    fclose(archivo_ordenado);
    heap_destruir(heap, heap_destruir_adapter);
    free_file_reader_vec(file_reader_vec, cant_particiones);
}

static bool juntar_ordenados(char *nombre_de_archivo, lista_t *particiones_nombres, cmp_linea_func_t cmp){

    FILE* archivo_ordenado = fopen(nombre_de_archivo, "w+");
    if(archivo_ordenado == NULL){
        return false;
    }

    heap_t* heap = heap_crear(cmp);
    if(heap == NULL){
        fclose(archivo_ordenado);
        return false;
    }

    file_reader_t** file_reader_vec = crear_file_reader_vec(particiones_nombres);
    if(file_reader_vec == NULL){
        fclose(archivo_ordenado);
        heap_destruir(heap, NULL);
        return false;
    }

    // guardar primer registro de cada archivo
    size_t cant_de_particiones = lista_largo(particiones_nombres);
    for(size_t j = 0; j < cant_de_particiones; j++){

        file_reader_t* file_reader = file_reader_vec[j];
        file_reader_leer(file_reader);
        if(file_reader->error){
            juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
            return false;
        }

        char* linea = file_reader->linea;
        heap_item_t* heap_item = heap_item_crear(linea, j);
        if(heap_item == NULL){
            juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
            return false;
        }

        bool encolar_ok = heap_encolar(heap, heap_item);
        if(!encolar_ok){
            juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
            return false;
        }
    }

    while(!heap_esta_vacio(heap)){
        heap_item_t* heap_item = heap_desencolar(heap);
        char* linea = heap_item->linea;
        size_t numero_de_archivo = heap_item->numero_archivo;

        int fputs_result = fputs(linea, archivo_ordenado);
        heap_item_destruir(heap_item);
        if(fputs_result == EOF){
            juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
            return false;
        }

        file_reader_t* file_reader = file_reader_vec[numero_de_archivo];
        file_reader_leer(file_reader);
        if(file_reader->error){
            juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
            return false;
        }

        if(!file_reader->eof){
            linea = file_reader->linea;
            heap_item = heap_item_crear(linea, numero_de_archivo);
            if(heap_item == NULL){
                juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
                return false;
            }
            bool encolado_ok = heap_encolar(heap, heap_item);
            if(!encolado_ok){
                juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
                return false;
            }
        }
    }

    juntar_ordenados_clean_up(archivo_ordenado, heap, file_reader_vec, cant_de_particiones);
    return true;
}

static bool borrar_particiones(lista_t* particiones){

    lista_iter_t* lista_iter = lista_iter_crear(particiones);
    if(lista_iter == NULL){
        return false;
    }

    bool borrados_ok = true;

    while(!lista_iter_al_final(lista_iter)){

        char* nombre_de_particion = lista_iter_ver_actual(lista_iter);
        int borrado = remove(nombre_de_particion);
        if(borrado == -1){
            borrados_ok = false;
        }

        lista_iter_avanzar(lista_iter);
    }

    lista_iter_destruir(lista_iter);
    return borrados_ok;
}

/* ******************************************************************
 *                heap_item_t: implementación
 * *****************************************************************/

static heap_item_t* heap_item_crear(char* linea, size_t numero_archivo){
    heap_item_t* heap_item = malloc(sizeof(heap_item_t));
    if(heap_item == NULL){
        return NULL;
    }

    char* copia_linea = str_copy(linea);
    if(copia_linea == NULL){
        return NULL;
    }

    heap_item->linea = copia_linea;
    heap_item->numero_archivo = numero_archivo;

    return heap_item;
}

static void heap_item_destruir(heap_item_t* heap_item){
    free(heap_item->linea);
    free(heap_item);
}

static int heap_item_comparar(const void *item1, const void *item2){

    const heap_item_t* item_1 = item1;
    const heap_item_t* item_2 = item2;
    char* l1 = item_1->linea;
    char* l2 = item_2->linea;

    return comparar_lineas_log(l1, l2) * -1;

}