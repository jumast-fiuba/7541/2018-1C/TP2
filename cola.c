#include "cola.h"
#include <stdlib.h>

/* Definición del struct node proporcionado por la cátedra.*/
typedef struct node {
	void* dato;
	struct node* prox;
	} nodo_t;

/* Definición del struct cola proporcionado por la cátedra.*/
struct cola {
    nodo_t* prim;  // Primer elemento almacenado en la cola.
    nodo_t* ult;   // Último elemento almacenado en la cola.
};

/* *****************************************************************
 *                    PRIMITIVAS DE LA COLA
 * *****************************************************************/

cola_t* cola_crear(void) {
	cola_t* cola = malloc(sizeof(cola_t));
	if (cola == NULL)
		return NULL;
	
	cola->prim = NULL;
	cola->ult = NULL;

	return cola;
}

void cola_destruir(cola_t *cola, void destruir_dato(void*)) {
	while (!cola_esta_vacia(cola)) {
		void* dato = cola_desencolar(cola);
		if (destruir_dato != NULL) {
			destruir_dato(dato);
		}
	}
	free(cola);
	return;
}

bool cola_esta_vacia(const cola_t *cola) {
	return (cola->prim == NULL);
}

bool cola_encolar(cola_t *cola, void* valor) {
	nodo_t* nodo_nuevo = malloc(sizeof(nodo_t));
	if (nodo_nuevo == NULL)
		return false;
	
	nodo_nuevo->dato = valor;
	nodo_nuevo->prox = NULL;

	if (cola_esta_vacia(cola))
		cola->prim = nodo_nuevo;
	else
		cola->ult->prox = nodo_nuevo; 
	
	cola->ult = nodo_nuevo;
	return true;
}

void* cola_ver_primero(const cola_t *cola) {
	if (cola_esta_vacia(cola))
		return NULL;

	return cola->prim->dato;
}

void* cola_desencolar(cola_t *cola) {
	if (cola_esta_vacia(cola))
		return NULL;
	
	void* valor_desencolado = cola_ver_primero(cola);
	nodo_t* aux_prox = cola->prim->prox;
	free(cola->prim);
	cola->prim = aux_prox;

	if (cola_esta_vacia(cola))
		cola->ult = NULL;

	return valor_desencolado;
}