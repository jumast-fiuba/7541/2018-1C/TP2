#include "heap.h"
#include <stdlib.h>
#include <stdbool.h>
#define CAPACIDAD_INICIAL 55

/* Definición del Heap */
struct heap {
    void** datos;
    size_t cantidad;
    size_t capacidad;
    cmp_func_t cmp;
};


/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/

/* Intercambia el elemento almacenado en dos posiciones de un arreglo */
void swap(void* arreglo[], size_t pos1, size_t pos2) { 
    void* aux_elem = arreglo[pos1];
    arreglo[pos1] = arreglo[pos2];
    arreglo[pos2] = aux_elem;
    return;
}


/* Aumenta o disminuye la capacidad del Heap según el "n" recibido por parámetro.
 * Devuelve true en caso de realizarse la redimensión, de lo contrario, devuelve false.
 */
bool heap_redimensionar(heap_t* heap, size_t n) {
	size_t capacidad_nueva = n;
	void** datos_nuevo = realloc(heap->datos, capacidad_nueva*sizeof(void*));
	if (datos_nuevo == NULL)
		return false;
	heap->capacidad = capacidad_nueva;
	heap->datos = datos_nuevo;
	return true;
}


void upheap(void** datos, size_t pos, cmp_func_t cmp) {
	if (pos == 0)
		return;
	size_t pos_padre = (pos-1)/2;
	
	if (cmp(datos[pos], datos[pos_padre])<0)
		return;
	
	swap(datos, pos, pos_padre);
	upheap(datos, pos_padre, cmp);
}


void downheap(void** datos, size_t n, size_t pos, cmp_func_t cmp) {
	if (pos >= n)
		return;
	size_t hijo_izq = 2*pos + 1;
	size_t hijo_der = 2*pos +2;
	size_t pos_max = pos;
	
	if (hijo_izq<n && cmp(datos[hijo_izq], datos[pos_max])>0)
		pos_max = hijo_izq;

	if (hijo_der<n && cmp(datos[hijo_der], datos[pos_max])>0)
		pos_max = hijo_der;

	if (pos!=pos_max) {
		swap(datos, pos, pos_max);
		downheap(datos, n, pos_max, cmp);
	}
	return;
}


/* Modifica un arreglo "in-place", dándole forma de heap de máximos
 * El primer elemento del arreglo será el máximo del mismo.
 */
void heapify(void* elementos[], size_t cant, cmp_func_t cmp) {
	for (size_t i = cant/2; i>0; i--)
		downheap(elementos, cant, i-1, cmp);
	return;
}

/* *****************************************************************
 *                    PRIMITIVAS DEL HEAP
 * *****************************************************************/

heap_t* heap_crear(cmp_func_t cmp) {
	heap_t* heap = malloc(sizeof(heap_t));
	if (heap == NULL)
		return NULL;
	
	heap->cantidad = 0;
	heap->capacidad = CAPACIDAD_INICIAL;
	heap->datos = malloc(CAPACIDAD_INICIAL*(sizeof(void*)));
	if (heap->datos == NULL) {
		free(heap);
		return NULL;
	}
	heap->cmp = cmp;
	return heap;
}


heap_t *heap_crear_arr(void *arreglo[], size_t n, cmp_func_t cmp) {
	heap_t* heap = malloc(sizeof(heap_t));
	if (heap == NULL)
		return NULL;	
	heap->cantidad = n;
	heap->capacidad = n;
	heap->cmp = cmp;	
	heap->datos = malloc(heap->capacidad*(sizeof(void*)));
	if (heap->datos == NULL) {
		free(heap);
		return NULL;
	}

	heapify(arreglo, n, cmp);
	for (int i = 0; i < n; i++)
		heap->datos[i] = arreglo[i];

	return heap; 
}


size_t heap_cantidad(const heap_t* heap) {
	return heap->cantidad;
}


bool heap_esta_vacio(const heap_t* heap) {
	return heap->cantidad==0;
}


void* heap_ver_max(const heap_t* heap) {
	if (heap_esta_vacio(heap))
		return NULL;
	return heap->datos[0];
}


bool heap_encolar(heap_t *heap, void *elem) {
	if (elem == NULL)
		return false;
	
	//Evaluar si hay que redimensionar
	if (heap->cantidad >= heap->capacidad) {
		bool redimension = heap_redimensionar(heap, heap->capacidad*2);
		if (!redimension)
			return false;
	}

	heap->datos[heap->cantidad] = elem;
	upheap(heap->datos, heap->cantidad, heap->cmp);
	heap->cantidad++;

	return true;
}


void *heap_desencolar(heap_t *heap) {	
	if (heap_esta_vacio(heap))
		return NULL;

	size_t pos_final = heap->cantidad-1;
	void* elem_a_desencolar = heap->datos[0];
	
	swap(heap->datos, 0, pos_final);
	heap->datos[pos_final] = NULL;
	heap->cantidad--;
	
	downheap(heap->datos, heap->cantidad, 0, heap->cmp);

	//Evaluar si hay que redimensionar
	if (heap->cantidad < heap->capacidad/4)
		heap_redimensionar(heap, heap->capacidad/2);
	
	return elem_a_desencolar;
}


void heap_destruir(heap_t *heap, void destruir_elemento(void *e)) {
	if (destruir_elemento) {
		for (int i = 0; i < heap->cantidad; i++)
			destruir_elemento(heap->datos[i]);
	}
	free(heap->datos);
	free(heap);
}


void heap_sort(void* elementos[], size_t cant, cmp_func_t cmp) {
	heapify(elementos, cant, cmp);
	size_t pos_ultimo_relativo = cant-1;

	for (; pos_ultimo_relativo>0; pos_ultimo_relativo--) {
		swap(elementos, 0, pos_ultimo_relativo);
		downheap(elementos, pos_ultimo_relativo, 0, cmp);
	}
	return;
}