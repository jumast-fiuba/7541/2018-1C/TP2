#include "lista.h"
#include <stdlib.h>


/* Definición del struct node */
typedef struct node {
	void* dato;
	struct node* prox;
} nodo_t;

/* Definición del struct lista */
struct lista {
    nodo_t* prim;  // Primer nodo de la lista.
    nodo_t* ult;   // Último nodo de la lista.
    size_t largo;  // Cantidad de nodos de la lista.
};

/* Definición del struct lista */
struct lista_iter {
    lista_t* lista;  // Lista sobre la que se itera
    nodo_t* anterior;  // Nodo anterior al apuntado por el iterador
    nodo_t* actual;   // Nodo al que apunta el iterador.
};


/* *****************************************************************
 *                    PRIMITIVAS DE LA LISTA ENLAZADA
 * *****************************************************************/

lista_t* lista_crear(void) {
	lista_t* lista = malloc(sizeof(lista_t));
	if (lista == NULL)
		return NULL;
	
	lista->prim = NULL;
	lista->ult = NULL;
	lista->largo = 0;
	
	return lista;
}


size_t lista_largo(const lista_t* lista) {
	return lista->largo;
}


bool lista_esta_vacia(const lista_t* lista) {
	return (lista->largo == 0);
}


bool lista_insertar_primero(lista_t* lista, void* dato) {
	nodo_t* nodo_nuevo = malloc(sizeof(nodo_t));
	if (nodo_nuevo == NULL)
		return false;
	
	nodo_nuevo->dato = dato;
	nodo_nuevo->prox = NULL;
	
	if (lista_esta_vacia(lista)) {
		lista->prim = nodo_nuevo;
		lista->ult = nodo_nuevo;
	} else {
		nodo_nuevo->prox = lista->prim;
		lista->prim = nodo_nuevo;
	}
	
	lista->largo ++;
	return true;
}

bool lista_insertar_ultimo(lista_t* lista, void* dato) {
	nodo_t* nodo_nuevo = malloc(sizeof(nodo_t));
	if (nodo_nuevo == NULL)
		return false;
	
	nodo_nuevo->dato = dato;
	nodo_nuevo->prox = NULL;
	
	if (lista_esta_vacia(lista)) {
		lista->prim = nodo_nuevo;
		lista->ult = nodo_nuevo;
	} else {
		lista->ult->prox = nodo_nuevo;
		lista->ult = nodo_nuevo;
	}
	
	lista->largo ++;
	return true;
}

void* lista_borrar_primero(lista_t* lista) {
	if (lista_esta_vacia(lista))
		return NULL;
	
	void* dato_borrado = lista_ver_primero(lista);
	nodo_t* aux_prox = lista->prim->prox;
	free(lista->prim);
	
	lista->prim = aux_prox;
	lista->largo --;
	
	if (lista_largo(lista) == 0)
		lista->ult = NULL;
	
	return dato_borrado;
}


void* lista_ver_primero(const lista_t* lista) {
	if (lista_esta_vacia(lista))
		return NULL;
	
	return lista->prim->dato;
}

void* lista_ver_ultimo(const lista_t* lista) {
	if (lista_esta_vacia(lista))
		return NULL;
	
	return lista->ult->dato;
}


void lista_destruir(lista_t* lista, void destruir_dato(void*)) {
	while (!lista_esta_vacia(lista)) {
		void* dato = lista_borrar_primero(lista);
		if (destruir_dato != NULL) {
			destruir_dato(dato);
		}
	}
	
	free(lista);
	return;
}


/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR EXTERNO
 * *****************************************************************/

lista_iter_t* lista_iter_crear(lista_t* lista) {
	lista_iter_t* iter = malloc(sizeof(lista_iter_t));
	if (iter == NULL)
		return NULL;

	iter->lista = lista;
	iter->anterior = NULL;
	iter->actual = lista->prim;

	return iter;
}


bool lista_iter_avanzar(lista_iter_t* iter) {
	if (lista_iter_al_final(iter))
		return false;
	iter->anterior = iter->actual;
	iter->actual = iter->anterior->prox;
	return true;
}


void* lista_iter_ver_actual(const lista_iter_t* iter) {
	if (lista_iter_al_final(iter)||iter->actual == NULL)
		return NULL;
	return iter->actual->dato;
}


bool lista_iter_al_final(const lista_iter_t* iter) {
	return (iter->actual == NULL);
}


bool lista_iter_insertar(lista_iter_t* iter, void* dato) {
	nodo_t* nodo_nuevo = malloc(sizeof(nodo_t));
	if (nodo_nuevo == NULL)
		return false;
	
	nodo_nuevo->dato = dato;
	nodo_nuevo->prox = NULL;
	
	if (iter->anterior == NULL)
		iter->lista->prim = nodo_nuevo;
	else
		iter->anterior->prox = nodo_nuevo;

	if (lista_iter_al_final(iter))
		iter->lista->ult = nodo_nuevo;
	
	nodo_nuevo->prox = iter->actual;
	iter->actual = nodo_nuevo;

	iter->lista->largo ++;
	return true;
}


void* lista_iter_borrar(lista_iter_t* iter) {
	if (lista_iter_al_final(iter))
		return NULL;

	void* dato_borrado = lista_iter_ver_actual(iter);

	if (iter->anterior == NULL) {
		iter->lista->prim = iter->actual->prox;
		free(iter->actual);
		iter->actual = iter->lista->prim;

	} else if (iter->actual->prox == NULL) {
		iter->anterior->prox = NULL;
		free(iter->actual);
		iter->actual = NULL;
		iter->lista->ult = iter->anterior;

	} else {
		iter->anterior->prox = iter->actual->prox;
		free(iter->actual);
		iter->actual = iter->anterior->prox;
	}

	iter->lista->largo --;
	return dato_borrado;	
}


void lista_iter_destruir(lista_iter_t* iter) {
	free(iter);
	return;
}

/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR INTERNO
 * *****************************************************************/

void lista_iterar(lista_t* lista, bool visitar(void* dato, void* extra), void* extra) {
	nodo_t* anterior = NULL;
	nodo_t* actual = lista->prim;

	while (actual != NULL) {
		if (visitar(actual->dato, extra) != true)
			return;
		anterior = actual;
		actual = anterior->prox;
	}
	return;
}

