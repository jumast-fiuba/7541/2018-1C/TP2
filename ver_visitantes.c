#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "strutil.h"
#include "abb.h"
#include "hash.h"
#include "cola.h"
#include "heap.h"
#include "lista.h"
#define TIME_FORMAT "%FT%T%z"
#include "utils.h"

void ver_visitantes(abb_t* ips, char* ip_inicial, char* ip_final) {
    abb_iter_t* iterar_ips = abb_iter_in_crear_desde_valor(ips, ip_inicial);
    if(iterar_ips == NULL){
        return;
    }

    fprintf(stdout, "Visitantes:\n");

    while (!abb_iter_in_al_final(iterar_ips)) {
        const char* actual = abb_iter_in_ver_actual(iterar_ips);
        if (comparar_ips(actual, ip_inicial) != -1 && comparar_ips(actual, ip_final) != 1)
            fprintf(stdout, "\t%s\n", actual);

        if (comparar_ips(actual, ip_final)==1)
            break;
        abb_iter_in_avanzar(iterar_ips);
    }

    abb_iter_in_destruir(iterar_ips);
}