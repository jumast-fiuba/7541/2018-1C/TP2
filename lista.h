#ifndef LISTA_H
#define LISTA_H

#include <stdlib.h>
#include <stdbool.h>


/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

/* La lista está planteada como una lista de punteros genéricos. */
struct lista;
typedef struct lista lista_t;

/* Iterador externo. */
struct lista_iter_t;
typedef struct lista_iter lista_iter_t;


/* ******************************************************************
 *                    PRIMITIVAS DE LA LISTA ENLAZADA
 * *****************************************************************/

// Crea una lista.
// Post: devuelve una nueva lista vacía.
lista_t* lista_crear(void);

// Devuelve verdadero o falso, según si el largo de la lista es mayor a 0 o no.
// Pre: la lista fue creada.
bool lista_esta_vacia(const lista_t* lista);

// Agrega un nuevo elemento a la lista. Devuelve falso en caso de error.
// Pre: la lista fue creada.
// Post: se agregó un nuevo elemento a la lista, el cual se encuentra al principio
// de la misma.
bool lista_insertar_primero(lista_t* lista, void* dato);

// Agrega un nuevo elemento a la lista. Devuelve falso en caso de error.
// Pre: la lista fue creada.
// Post: se agregó un nuevo elemento a la lista, el cual se encuentra al final
// de la misma. La lista contiene un elemento más.
bool lista_insertar_ultimo(lista_t* lista, void* dato);

// Saca el primer elemento de la lista. Si la lista tiene elementos, se quita el
// primero de la mista, y se devuelve su valor, si está vacía, devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el valor del primer elemento anterior, la lista
// contiene un elemento menos, si no estaba vacía.
void* lista_borrar_primero(lista_t* lista);

// Obtiene el valor del primer elemento de la lista. Si la lista tiene
// elementos, se devuelve el valor del primero, si está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el primer elemento de la lista, cuando no está vacía.
void* lista_ver_primero(const lista_t* lista);

// Obtiene el valor del último elemento de la lista. Si la lista tiene
// elementos, se devuelve el valor del último, si está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el último elemento de la lista, cuando no está vacía.
void* lista_ver_ultimo(const lista_t* lista);

// Obtiene el número de elementos de la lista
// Pre: la lista fue creada.
// Post: se devolvió la cantidad de elementos de la lista, si está vacía,
// devuelve 0.
size_t lista_largo(const lista_t* lista);

// Destruye la lista. Si se recibe la función destruir_dato por parámetro,
// para cada uno de los elementos de la cola lista a destruir_dato.
// Pre: la lista fue creada. destruir_dato es una función capaz de destruir
// los datos de la lista, o NULL en caso de que no se la utilice.
// Post: se eliminaron todos los elementos de la lista.
void lista_destruir(lista_t* lista, void destruir_dato(void*));



/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR EXTERNO
 * *****************************************************************/

// Crea un iterador de una lista, la cual recibe por parámetro
// Pre: la lista fue creada
// Post: devuelve un iterador.
lista_iter_t* lista_iter_crear(lista_t* lista);

// Apunta el iterador al siguiente elemento de la lista, devolviendo true. 
// Si ya se encuentra al final de la lista, devuelve false.
// Pre: el iterador fue creado.
// Post: devuelve true si el iterador ahora apunta al siguiente elemento
// de la lista. En caso de estar al final de la lista, devuelve false.
bool lista_iter_avanzar(lista_iter_t* iter);

// Obtiene el valor del elemento sobre el cual se encuentra el iterador. 
// Si la lista esta vacía o el iterador llegó al final, devuelve NULL.
// Pre: El iterador fue creado.
// Post: se devolvió el elemento sobre el cual se encuentra el iterador,
// cuando no está vacía o el iterador no está al final.
void* lista_iter_ver_actual(const lista_iter_t* iter);

// Muestra si el iterador está situado al final de la lista. 
// Devuelve true en caso de ser así. En caso contrario, devuelve false. 
// Pre: el iterador fue creado.
// Post: Devuelve true si el iterador está al final de la lista. de lo contrario, false.
bool lista_iter_al_final(const lista_iter_t* iter);

// Inserta un nuevo elemento a la lista, en la posición actual del iterador. 
// Devuelve falso en caso de error.
// Pre: el iterador fue creado.
// Post: se insertó un nuevo elemento a la lista, el cual se encuentra en la 
// posición actual del iterador.
bool lista_iter_insertar(lista_iter_t* iter, void* dato);

// Borra el elemento de la lista que está en la posición actual del iterador.
// Si está vacía, devuelve NULL.
// Pre: el iterador fue creado.
// Post: se devolvió el valor del elemento borrado, la lista
// contiene un elemento menos, si no estaba vacía.
void* lista_iter_borrar(lista_iter_t* iter);

// Destruye el iterador.
// Pre: el iterador fue creado.
// Post: el iterador fue destruído.
void lista_iter_destruir(lista_iter_t* iter);


/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR INTERNO
 * *****************************************************************/

// Recorre la lista. Recibe una función visitar por parámetro y la aplica a algunos o 
// todos los elementos de la lista, de acuerdo a cómo esté implementada la función. 
// Pre: la lista fue creada. visitar es una función que devuelve un booleano.
// Post: se aplicó la función visitar a los elementos de la lista.
void lista_iterar(lista_t* lista, bool visitar(void* dato, void* extra), void* extra);


/* *****************************************************************
 *                      PRUEBAS UNITARIAS
 * *****************************************************************/

// Realiza pruebas sobre la implementación del alumno.
//
// Las pruebas deben emplazarse en el archivo ‘pruebas_alumno.c’, y
// solamente pueden emplear la interfaz pública tal y como aparece en lista.h
// (esto es, las pruebas no pueden acceder a los miembros del struct lista).
//
// Para la implementación de las pruebas se debe emplear la función
// print_test(), como se ha visto en TPs anteriores.
void pruebas_lista_alumno(void);

#endif // LISTA_H
