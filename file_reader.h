#ifndef TP2_FILE_READER_H
#define TP2_FILE_READER_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct file_reader{
    FILE* archivo;
    char* linea;
    size_t size_in_bytes;
    size_t leidos;
    bool eof;
    bool error;
} file_reader_t;

file_reader_t* file_reader_crear(char* nombre_de_archivo);
void file_reader_destruir(file_reader_t* file_reader);
bool file_reader_leer(file_reader_t* file_reader);

#endif //TP2_FILE_READER_H


