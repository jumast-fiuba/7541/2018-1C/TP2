
#include "abb.h"
#include "pila.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
/* Definición del nodo ABB */
typedef struct nodo_abb {
	struct nodo_abb* izq;
	struct nodo_abb* der;
	char* clave;
	void* dato;  
} nodo_abb_t;

/* Definición del ABB */
struct abb {
    nodo_abb_t* raiz;
    abb_comparar_clave_t cmp;
    abb_destruir_dato_t destruir_dato;
    size_t cantidad;
};

/* Definición del iterador externo del ABB */
struct abb_iter {
	const abb_t* arbol;
	pila_t* orden;
};


/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/

/* Crea un nodo ABB, almacenando una copia de la clave y el dato. */
nodo_abb_t* crear_nodo(const char* clave, void* dato) {
	nodo_abb_t* nodo = malloc(sizeof(nodo_abb_t));
	if (nodo == NULL)
		return NULL;
	
	nodo->izq = NULL;
	nodo->der = NULL;
	nodo->clave = malloc(strlen(clave)+1);
	if (nodo->clave == NULL) {
		free(nodo);
		return NULL;
	}
	strcpy(nodo->clave, clave);
	nodo->dato = dato;

	return nodo;
}


/* Devuelve el nodo que contiene la clave recibida por parámetro.
 * Si la clave no está, devuelve NULL.
 */
nodo_abb_t* buscar_nodo(const abb_t* arbol, nodo_abb_t* nodo, const char* clave) {
	if (nodo==NULL)
		return NULL;
	
	if (arbol->cmp(nodo->clave, clave) <0) {           //Nodo está a la derecha del árbol
		nodo = buscar_nodo(arbol, nodo->der, clave);
	} else if (arbol->cmp(nodo->clave, clave) >0)      //Nodo está a la izquierda del árbol
		nodo = buscar_nodo(arbol, nodo->izq, clave);   

	return nodo;
}


/* Devuelve el padre de un nodo.
 * Si el nodo es la raíz, devuelve NULL
 */
nodo_abb_t* buscar_padre(const abb_t* arbol, nodo_abb_t* nodo, const char* clave) {
	nodo_abb_t* nodo_hijo;
	if (nodo==NULL || arbol->cmp(arbol->raiz->clave, clave)==0)
		return NULL;

	if (arbol->cmp(nodo->clave, clave) <0) {
		nodo_hijo = nodo->der;
	} else
		nodo_hijo = nodo->izq;

	if (nodo_hijo==NULL || arbol->cmp(nodo_hijo->clave, clave)==0)
		return nodo;
	return buscar_padre(arbol, nodo_hijo, clave);
}

/* Intercambia la clave y el dato de dos nodos */
void swap_abb(nodo_abb_t* nodo1, nodo_abb_t* nodo2) {
	char* aux_clave = nodo1->clave; 
	void* aux_dato = nodo1->dato;

	nodo1->clave = nodo2->clave;
	nodo1->dato = nodo2->dato;
	nodo2->clave = aux_clave;
	nodo2->dato = aux_dato;

	return;
}

/* Borra del ABB un nodo que no tiene hijos.
*/
void* abb_borrar_hoja(abb_t* arbol, nodo_abb_t* padre, nodo_abb_t* nodo) {
	void* dato_a_borrar = nodo->dato;
	if (padre==NULL) {
		arbol->raiz = NULL;
	} else if (padre->izq==nodo) {
		padre->izq=NULL;
	} else 
		padre->der=NULL;

	free(nodo->clave);
	free(nodo);
	return dato_a_borrar;
}

/* Borra del ABB un nodo con un solo hijo.
*/
void* abb_borrar_con_un_hijo(abb_t* arbol, nodo_abb_t* padre, nodo_abb_t* nodo) {
	nodo_abb_t* hijo;
	void* dato_a_borrar = nodo->dato;

	if (nodo->izq!=NULL) {
		hijo = nodo->izq;
	} else
		hijo = nodo->der;

	if (nodo!=arbol->raiz) {
		if (padre->izq==nodo) {
			padre->izq = hijo;
		} else
			padre->der = hijo;
	} else 
		arbol->raiz = hijo;

	free(nodo->clave);
	free(nodo);
	return dato_a_borrar;
}

/* Borra del ABB un nodo con dos hijos.
*/
void* abb_borrar_con_dos_hijos(abb_t* arbol, nodo_abb_t* nodo) {
	nodo_abb_t* nodo_mas_chico = nodo->der;
	while (nodo_mas_chico->izq!=NULL)
		nodo_mas_chico = nodo_mas_chico->izq;

	if (nodo_mas_chico == nodo->der) {
		swap_abb(nodo, nodo_mas_chico);
		if (nodo_mas_chico->der!=NULL)
			return abb_borrar_con_un_hijo(arbol, nodo, nodo_mas_chico);
		return abb_borrar_hoja(arbol, nodo, nodo_mas_chico);
	}

	nodo_abb_t* padre = buscar_padre(arbol, arbol->raiz, nodo_mas_chico->clave);
	swap_abb(nodo_mas_chico, nodo);

	if (nodo_mas_chico->der!=NULL)
		return abb_borrar_con_un_hijo(arbol, padre, nodo_mas_chico);
	return abb_borrar_hoja(arbol, padre, nodo_mas_chico);
}



/* *****************************************************************
 *                    PRIMITIVAS DEL ABB
 * *****************************************************************/

abb_t *abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato) {
	abb_t* arbol = malloc(sizeof(abb_t));
	if (arbol == NULL)
		return NULL;
	
	arbol->raiz = NULL;
	arbol->cantidad = 0;
	arbol->cmp = cmp;
	arbol->destruir_dato = destruir_dato;

	return arbol;
}


bool abb_guardar(abb_t *arbol, const char *clave, void *dato) {
	nodo_abb_t* nodo_a_guardar;
	//Si la clave ya está en el arbol, reemplazo dato
	if (abb_pertenece(arbol, clave)) {
		nodo_a_guardar = buscar_nodo(arbol, arbol->raiz, clave);
		if (arbol->destruir_dato)
			arbol->destruir_dato(nodo_a_guardar->dato);
		nodo_a_guardar->dato = dato;
		return true;
	}

	//Si la clave no está en el arbol
	nodo_abb_t* padre = buscar_padre(arbol, arbol->raiz, clave);
	nodo_a_guardar = crear_nodo(clave, dato);
	
	if (padre==NULL) {
		arbol->raiz = nodo_a_guardar;		
	} else if (arbol->cmp(padre->clave, clave) <0) {   //Hijo derecho
		padre->der = nodo_a_guardar;
	} else                                             //Hijo izquierdo
		padre->izq = nodo_a_guardar;

	arbol->cantidad++;
	return true;
}


void* abb_borrar(abb_t* arbol, const char* clave) {
	if (!abb_pertenece(arbol, clave))
		return NULL;
	nodo_abb_t* nodo = buscar_nodo(arbol, arbol->raiz, clave);
	nodo_abb_t* padre = buscar_padre(arbol, arbol->raiz, clave);
	arbol->cantidad--;

	//Si el nodo no tiene hijos
	if (nodo->izq==NULL && nodo->der==NULL)
		return abb_borrar_hoja(arbol, padre, nodo);

	//Si el nodo tiene un hijo
	if (nodo->izq==NULL || nodo->der==NULL)
		return abb_borrar_con_un_hijo(arbol, padre, nodo);

	//Si el nodo tiene dos hijos
	return abb_borrar_con_dos_hijos(arbol, nodo);
}


void* abb_obtener(const abb_t* arbol, const char* clave) {
	if (!abb_pertenece(arbol, clave))
		return NULL;
	nodo_abb_t* nodo = buscar_nodo(arbol, arbol->raiz, clave); 
	return nodo->dato;
}


bool abb_pertenece(const abb_t* arbol, const char* clave) {
	return buscar_nodo(arbol, arbol->raiz, clave);
}


size_t abb_cantidad(abb_t* arbol) {
	return arbol->cantidad;
}


void _abb_destruir(abb_t* arbol, nodo_abb_t* nodo) {
	if (nodo->izq!=NULL)
		_abb_destruir(arbol, nodo->izq);
	if (nodo->der!=NULL)
		_abb_destruir(arbol, nodo->der);

	if (arbol->destruir_dato)
		arbol->destruir_dato(nodo->dato);
	free(nodo->clave);
	free(nodo);
	return;
}

void abb_destruir(abb_t *arbol) {
	if (abb_cantidad(arbol)!=0)
		_abb_destruir(arbol, arbol->raiz);
	free(arbol);
	return;
}



/* *****************************************************************
 *            PRIMITIVAS DEL ITERADOR EXTERNO DEL ABB
 * *****************************************************************/

//FUNCION AUXILIAR
/* Apila recursivamente el hijo izquierdo de un nodo pasado por parámetro.
*/
void apilar_izquierdos(abb_iter_t* iter, nodo_abb_t* nodo) {
	pila_apilar(iter->orden, nodo);
	nodo_abb_t* izquierdo = nodo->izq;
	while (izquierdo!=NULL) {
		pila_apilar(iter->orden, izquierdo);
		izquierdo = izquierdo->izq;
	}
	return;
}


//FUNCIÓN AUXILIAR
/* Apila recursivamente el hijo izquierdo de un nodo, comparándolo con un valor
 * recibido por parámetro, cuando este valor es mayor o no hay más hijos izquierdos,
 * deja de apilar.
 */
void apilar_izquierdos_desde_valor(abb_iter_t* iter, nodo_abb_t* nodo, const char* valor) {
	pila_apilar(iter->orden, nodo);
	nodo_abb_t* izquierdo = nodo->izq;
	while (izquierdo!=NULL) {
		if (iter->arbol->cmp(izquierdo->clave, valor)<0)
			break;
		pila_apilar(iter->orden, izquierdo);
		izquierdo = izquierdo->izq;
	}
	return;
}


abb_iter_t* abb_iter_in_crear(const abb_t* abb) {
	abb_iter_t* iter = malloc(sizeof(abb_iter_t));
	if (iter==NULL)
		return NULL;
	
	iter->arbol = abb;
	iter->orden = pila_crear();
	if (iter->orden==NULL) {
		free(iter);
		return NULL;
	}
	
	if (abb->cantidad==0) 
		return iter;

	apilar_izquierdos(iter, iter->arbol->raiz);
	return iter;
}

//static nodo_abb_t* obtener_minimo(nodo_abb_t *nodo){
//    while(nodo->izq != NULL){
//        nodo = nodo->izq;
//    }
//    return nodo;
//}

//nodo_abb_t* buscar_nodo_bis(const abb_t* arbol, nodo_abb_t* nodo, const char* clave) {
//	if (nodo==NULL)
//		return NULL;
//
//	int cmp = arbol->cmp(nodo->clave, clave);
//
//
//	if (arbol->cmp(nodo->clave, clave) <0) {           //Nodo está a la derecha del árbol
//		nodo = buscar_nodo(arbol, nodo->der, clave);
//	} else if (arbol->cmp(nodo->clave, clave) >0)      //Nodo está a la izquierda del árbol
//		nodo = buscar_nodo(arbol, nodo->izq, clave);
//
//	return nodo;
//}

/* Crea un iterador in-order del ABB, recorriéndolo desde una clave recibida por
 * parámetro.
 */
abb_iter_t* abb_iter_in_crear_desde_valor(abb_t* abb, const char* valor) {

	if(abb_pertenece(abb, valor)){
		return abb_iter_in_crear(abb);
	}

	abb_guardar(abb, valor, NULL);
	abb_iter_t* abb_iter = abb_iter_in_crear(abb);
	abb_iter_in_avanzar(abb_iter);
	abb_borrar(abb, valor);
	return abb_iter;


//	abb_iter_t* iter = malloc(sizeof(abb_iter_t));
//	if (iter==NULL)
//		return NULL;
//
//	iter->arbol = abb;
//	iter->orden = pila_crear();
//	if (iter->orden==NULL) {
//		free(iter);
//		return NULL;
//	}
//
//	if (abb->cantidad==0)
//		return iter;
//
//	if (iter->arbol->cmp(iter->arbol->raiz->clave, valor)>0) {
//		apilar_izquierdos_desde_valor(iter, iter->arbol->raiz, valor);
//	} else {
//		nodo_abb_t* nodo_act = iter->arbol->raiz;
//		while (iter->arbol->cmp(nodo_act->clave, valor)<0)
//			nodo_act = nodo_act->der;
//		apilar_izquierdos(iter, nodo_act);
//	}
//	return iter;
}


bool abb_iter_in_avanzar(abb_iter_t* iter) {
	if (abb_iter_in_al_final(iter))
		return false;
	
	nodo_abb_t* desapilado = (nodo_abb_t*)pila_desapilar(iter->orden);
	
	if (desapilado->der!=NULL)
		apilar_izquierdos(iter, desapilado->der);

	return true;
}


const char* abb_iter_in_ver_actual(const abb_iter_t* iter) {
	if (abb_iter_in_al_final(iter))
		return NULL;
	nodo_abb_t* actual = (nodo_abb_t*)pila_ver_tope(iter->orden); 
	return actual->clave;
}


bool abb_iter_in_al_final(const abb_iter_t* iter) {
	return pila_esta_vacia(iter->orden);
}


void abb_iter_in_destruir(abb_iter_t* iter) {
	pila_destruir(iter->orden);
	free(iter);
	return;
}



/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR INTERNO
 * *****************************************************************/

/* Recorre el ABB in order. Recibe una función visitar por parámetro y la aplica a 
 * algunos o todos los elementos del arbol, de acuerdo a cómo esté implementada la misma. 
 * También recibe un puntero a size_t iterar; 'iterar' cambia según el valor de retorno de 
 * la función visitar: Cuando ésta devuelve false, el valor de iterar cambia a 1 y finaliza 
 * la iteración.
 */
void _abb_in_order(nodo_abb_t* nodo, bool visitar(const char* clave, void* valor, void* extra), void* extra, size_t* iterar) {
	if (nodo==NULL)
		return;
	
	_abb_in_order(nodo->izq, visitar, extra, iterar);
	if (*iterar==1)
		return;
	
	if (!visitar(nodo->clave, nodo->dato, extra)) {
		*iterar = 1;
		return;
	}
	
	_abb_in_order(nodo->der, visitar, extra, iterar);
	return;
}


void abb_in_order(abb_t* arbol, bool visitar(const char* clave, void* valor, void* extra), void* extra) {
	size_t iterar = 0;
	_abb_in_order(arbol->raiz, visitar, extra, &iterar);
	return;
}
