#ifndef TP2_PARTICION_H
#define TP2_PARTICION_H

typedef struct{
    char* nombre_archivo;
    size_t max_size_in_bytes;
    lista_t* lineas;
    size_t size_in_bytes;
} particion_t;

particion_t* particion_crear(char* nombre_de_archivo, size_t numero_de_particion, size_t max_size_in_bytes);
void particion_destruir(particion_t* particion);
char* particion_nombre_de_archivo(particion_t* particion);
bool particion_acepta_linea(particion_t* particion, const char* linea);
bool particion_agregar_linea(particion_t* particion, char* linea);
bool particion_guardar_ordenada(particion_t* particion, cmp_linea_func_t cmp);
bool particion_guardar(particion_t* particion);

#endif //TP2_PARTICION_H
