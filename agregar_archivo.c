#define _POSIX_C_SOURCE 200809L
#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "strutil.h"
#include "abb.h"
#include "hash.h"
#include "cola.h"
#include "heap.h"
#include "lista.h"
#define TIME_FORMAT "%FT%T%z"
#include "utils.h"
#include "file_reader.h"

#define DoS_PETICIONES 5
#define DoS_SEGUNDOS 2


/* ******************************************************************
 *                Declaración de funciones auxiliares
 * *****************************************************************/

// Dada una cadena en formato ISO-8601 devuelve una variable de tipo
// time_t que representa un instante en el tiempo
static time_t iso8601_to_time(const char* iso8601);

/* Evalúa las fechas de acceso de las direcciones IP recibidas por parámetro
 * (guardadas en un Hash). Si una dirección IP realizó cinco o más peticiones
 * en menos de dos segundos, se alerta por salida estánda como sospechosa de
 * intento de DoS.
 */
static bool detectar_dos(hash_t* direcciones);

/* ******************************************************************
 *                        AGREGAR ARCHIVO
 * *****************************************************************/

static void hash_destruir_dato_adapter(void *dato){
    lista_t* lista = dato;
    lista_destruir(lista, free);
}

bool agregar_archivo(abb_t* ips, char* nombre_archivo) {
    file_reader_t* file_reader = file_reader_crear(nombre_archivo);
    if(file_reader == NULL){
        return false;
    }

    hash_t* direcciones = hash_crear(hash_destruir_dato_adapter);
    if(direcciones == NULL){
        file_reader_destruir(file_reader);
        return false;
    }

    bool lectura_ok = file_reader_leer(file_reader);
    if(!lectura_ok){
        file_reader_destruir(file_reader);
        hash_destruir(direcciones);
        return false;
    }

    //Agregar visitantes
    while (!file_reader->eof) {
        char** linea_split = split(file_reader->linea, '\t');
        if(linea_split == NULL){
            file_reader_destruir(file_reader);
            hash_destruir(direcciones);
            return false;
        }

        char* ip = linea_split[0];

        char* fecha_copia = str_copy(linea_split[1]);
        if(fecha_copia == NULL){
            file_reader_destruir(file_reader);
            hash_destruir(direcciones);
            free_strv(linea_split);
            return false;
        }

        bool guardar_ip_ok = abb_guardar(ips, ip, NULL);   //Guarda IP en ABB
        if(!guardar_ip_ok){
            file_reader_destruir(file_reader);
            hash_destruir(direcciones);
            free_strv(linea_split);
            free(fecha_copia);
            return false;
        }

        lista_t* fechas_acceso;
        if (hash_pertenece(direcciones, ip)) {
            fechas_acceso = hash_obtener(direcciones, ip);
        }
        else{
            fechas_acceso = lista_crear();
            bool guardado_ok = hash_guardar(direcciones, ip, fechas_acceso);
            if(fechas_acceso == NULL || !guardado_ok){
                file_reader_destruir(file_reader);
                hash_destruir(direcciones);
                free_strv(linea_split);
                free(fecha_copia);
                return false;
            }
        }
        //Guardar la última fecha de ingreso desde una dirección IP
        bool insertar_ok = lista_insertar_ultimo(fechas_acceso, fecha_copia);
        if(!insertar_ok){
            file_reader_destruir(file_reader);
            hash_destruir(direcciones);
            free_strv(linea_split);
            free(fecha_copia);
            return false;
        }

        free_strv(linea_split);

        file_reader_leer(file_reader);
        if(file_reader->error){
            file_reader_destruir(file_reader);
            hash_destruir(direcciones);
            free_strv(linea_split);
            free(fecha_copia);
            return false;
        }
    }

    bool detectar_dos_ok = detectar_dos(direcciones);

    file_reader_destruir(file_reader);
    hash_destruir(direcciones);

    return detectar_dos_ok;
}

/* ******************************************************************
 *               Implementación de funciones auxiliares
 * *****************************************************************/

static time_t iso8601_to_time(const char* iso8601) {
    struct tm bktime = { 0 };
    strptime(iso8601, TIME_FORMAT, &bktime);
    return mktime(&bktime);
}

static bool detectar_dos(hash_t* direcciones) {
    hash_iter_t* iter_direcciones = hash_iter_crear(direcciones);
    if(iter_direcciones == NULL){
        return false;
    }

    heap_t* DoS = heap_crear(comparar_ips_invertida);
    if(DoS == NULL){
        hash_iter_destruir(iter_direcciones);
        return false;
    }

    while (!hash_iter_al_final(iter_direcciones)) {
        const char* ip_actual = hash_iter_ver_actual(iter_direcciones);
        lista_t* fechas = hash_obtener(direcciones, ip_actual);
        if (lista_largo(fechas)<DoS_PETICIONES) {
            hash_iter_avanzar(iter_direcciones);
            continue;
        }

        //Iteradores para las fechas
        lista_iter_t* iter_fecha_1 = lista_iter_crear(fechas);
        if(iter_fecha_1 == NULL){
            hash_iter_destruir(iter_direcciones);
            heap_destruir(DoS, free);
            return false;
        }

        lista_iter_t* iter_fecha_2 = lista_iter_crear(fechas);
        if(iter_fecha_2 == NULL){
            hash_iter_destruir(iter_direcciones);
            heap_destruir(DoS, free);
            lista_iter_destruir(iter_fecha_1);
            return false;
        }

        //Situa al segundo iterador a 5 nodos de distancia
        for (int i = 1; i < DoS_PETICIONES; i++)
            lista_iter_avanzar(iter_fecha_2);

        while (!lista_iter_al_final(iter_fecha_2)) {
            time_t fecha_1 = iso8601_to_time((char*)lista_iter_ver_actual(iter_fecha_1));
            time_t fecha_2 = iso8601_to_time((char*)lista_iter_ver_actual(iter_fecha_2));

            if (difftime(fecha_2, fecha_1) < DoS_SEGUNDOS) {
                heap_encolar(DoS, (void*)ip_actual);
                break;
            }
            lista_iter_avanzar(iter_fecha_1);
            lista_iter_avanzar(iter_fecha_2);
        }

        lista_iter_destruir(iter_fecha_1);
        lista_iter_destruir(iter_fecha_2);
        hash_iter_avanzar(iter_direcciones);
    }

    while (!heap_esta_vacio(DoS))
        fprintf(stdout, "DoS: %s\n", (char*)heap_desencolar(DoS));

    hash_iter_destruir(iter_direcciones);
    heap_destruir(DoS, NULL);

    return true;
}