#include "strutil.h"
#include <stdlib.h>
#include <string.h>

char** split(const char* str, char sep) {
	if (sep == '\0')
		return NULL;

	size_t len_str = strlen(str);    //longitud de la cadena recibida por parámetro
	char** cadenas = (char**)malloc(sizeof(char*)*(len_str+2));
	if (cadenas == NULL)
		return NULL;
	
	size_t i = 0;         //posición en el arreglo cadenas
	cadenas[i] = NULL;
	
	for (size_t pos_str = 0; pos_str <= len_str; pos_str++) {
		cadenas[i+1] = NULL;

		cadenas[i] = malloc(sizeof(char)*(len_str-pos_str+1));
		if (cadenas[i] == NULL)
			return NULL;

		int j = 0;  

		while ((str[pos_str]!=sep) && (pos_str<len_str)) {
			if (str[pos_str]=='\n')
				break;
			cadenas[i][j] = str[pos_str];
			pos_str++;
			j++;
		}

		cadenas[i][j] = '\0';
		i++;
	}
	return cadenas;   
}


char* join(char** strv, char sep) {
	char* cadena;

	if (sep == '\0')
		return NULL;
	if (strv == NULL) {
		cadena = malloc(sizeof(char));
		if (cadena == NULL)
			return NULL;
		cadena[0] = '\0';
		return cadena;
	}

	size_t len_str = 0;     //longitud de la cadena resultante
	size_t i;               //posición en el arreglo dinámico de cadenas recibido por parámetro      

	for (i = 0; strv[i]!=NULL; i++)
		len_str += strlen(strv[i]);

	cadena = malloc(sizeof(char)*(len_str+i+1));
	if (cadena == NULL)
		return NULL;

	size_t pos_str = 0;     //posición en la cadena resultante
	
	i = 0;
	while (strv[i]!=NULL) {
		int j = 0;
		
		while (strv[i][j]!='\0') {
			cadena[pos_str] = strv[i][j];
			pos_str++;
			j++;
		}
		
		i++;
		if (strv[i]!=NULL) {
			cadena[pos_str] = sep;
			pos_str++;
		}
	}

	cadena[pos_str] = '\0';
	return cadena;
}


void free_strv(char* strv[]) {
	for (int i = 0; strv[i]!=NULL; i++)
		free(strv[i]);
	free(strv);
	return;
}